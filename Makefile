.PHONY: check
check: ffmpeg.check

media/ccc.mp4:
	wget https://cdn.media.ccc.de/contributors/berlin/datengarten/h264-hd/datengarten-90-deu-Datengarten_90_hd.mp4 -O ccc.mp4

ffmpeg.check: media/ccc.mp4
	test/ffmpeg > ffmpeg.check

